FROM node:12-alpine

RUN apk add git make

RUN mkdir -p /opt/app
WORKDIR /opt/app


ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/app/node_modules/.bin

ADD .npmrc /opt/app/
ADD package.json /opt/app/
ADD package-lock.json /opt/app/
RUN npm i

ADD scripts /opt/app/scripts
ADD src /opt/app/src
ADD .eslintrc.yaml /opt/app/.eslintrc.yaml
ADD Makefile /opt/app/Makefile
ADD ecosystem.config.js /opt/app/ecosystem.config.js

COMPOSE_RUN = docker-compose run --rm
APP = $(COMPOSE_RUN) app
TEST = $(COMPOSE_RUN) test


LINT_ARGS = -- --fix


build:
	docker-compose build
start:
	docker-compose up; docker-compose kill
lint:
	$(TEST) npm run 'test:lint' $(LINT_ARGS)
unittest:
	$(TEST) npm run 'test:unit'
generate_jwks:
	$(APP) npm run 'jwt:generate_jwks'

db_repl:
	$(APP) npm run 'repl:db'


ci: APP =
ci: TEST =
ci: LINT_ARGS =
ci: lint unittest

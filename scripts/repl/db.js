#!/usr/bin/env node

const _ = require('lodash');
const moment = require('moment-timezone');
const repl = require('repl');

const diDb = require('../../src/di/db');
const { applyPasswordHash } = require('../../src/models/member');

const tz = 'Australia/Melbourne';

const stateLogAddDates = stateLog => {
  let lastDate = moment.tz(tz).subtract(1, 'day');
  for (const entry of stateLog) {
    if (!entry.date) {
      entry.date = lastDate.clone().add(1);
    }
    lastDate = entry.date;
  }
  return stateLog;
};

const adminName = 'biru';
const statePresets = {
  member: async applyDate => ([
    { action: 'SUBMIT_APPLICATION', date: applyDate },
    { action: 'PAY' },
    { action: 'RAISE' },
    { action: 'APPROVE' },
  ]),
};
statePresets.resigned = async applyDate => ([
  ...(await statePresets.member(applyDate)),
  { action: 'RESIGN' },
]);
statePresets.expelled = async applyDate => ([
  ...(await statePresets.member(applyDate)),
  { action: 'EXPEL' },
]);
statePresets.paymentDue = async () => {
  const lastYear = moment.tz(tz).subtract(1, 'year');
  return statePresets.member(lastYear);
};
statePresets.doublePaid = async () => {
  const membershipRollover = moment.tz(tz).startOf('year');
  const date = membershipRollover.clone().subtract(1);
  const effectiveDate = membershipRollover.clone().add(1);

  // Payment due creates a member who paid a year ago
  const stateLog = await statePresets.paymentDue();
  // Add a payment at the end of last year (double paid last year)
  stateLog.push({ action: 'PAY', date, effectiveDate });

  return stateLog;
};
/* statePresets.imported = async () => ([
]).map(i => ({ ...i, date: moment.tz(i.date, tz) })); */

const getStatePreset = async (name, ...args) => (
  stateLogAddDates(await statePresets[name](...args))
    .map(entry => ({
      ...entry,
      date: entry.date.toDate(),
      ...(
        entry.effectiveDate
          ? { effectiveDate: entry.effectiveDate.toDate() }
          : {}
      ),
    }))
);

const adminPresets = {
  init: async applyDate => ({
    email: 'biru@vicpah.org.au',
    emailVerified: true,
    hideMemberList: false,
    scope: ['committee', 'secretary'],
    displayName: 'Biru',
    givenName: 'Abc',
    familyName: 'Def',
    pronouns: 'he_him',
    mobileNumber: '0400000000',
    address: '1 Pig St\nPiggeston, 3000',
    stateLog: await getStatePreset('member', applyDate),
  }),
};

const run = async () => {
  const iface = repl.start({
    prompt: 'firestore> ',
  });
  const db = diDb.getDb();
  const membersRef = diDb.getMembersRef();
  const adminRef = membersRef.doc(`${adminName}0000000000000000`);
  Object.assign(iface.context, {
    _,
    db,
    membersRef,
    moment,
    [adminName]: {
      ref: adminRef,
      init: async (override = {}, ...args) => (
        adminRef.create({ ...(await adminPresets.init(...args)), ...override })
      ),
      setPassword: async password => (
        adminRef.update(applyPasswordHash({
          password,
	  email: (await adminRef.get()).data().email,
	}))
      ),
      setScope: async scope => (
        adminRef.update({ scope })
      ),
      setPreset: async (presetName, override = {}, ...args) => (
        adminRef.set({ ...(await adminPresets[presetName](...args)), ...override })
      ),
      setStatePreset: async (presetName, ...args) => (
        adminRef.update({ stateLog: await getStatePreset(presetName, ...args) })
      ),
      getStatePresets: async () => {
        const rv = {};
        for (const k of Object.keys(statePresets)) {
          rv[k] = await getStatePreset(k);
        }
        return rv;
      },
    },
  });
};

run();

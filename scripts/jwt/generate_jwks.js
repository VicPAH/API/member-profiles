#!/usr/bin/env node

const Eckles = require('eckles'); // eslint-disable-line import/no-extraneous-dependencies

/* eslint-disable no-console */
Eckles
  .generate({ format: 'jwk' })
  .then(keypair => {
    console.log(
      'member_profiles_jwk_b64:',
      Buffer.from(JSON.stringify(keypair)).toString('base64'),
    );
    console.log(
      'website_jwks_b64:',
      Buffer.from(JSON.stringify({
        keys: [{
          use: 'sig',
          ...keypair.public,
        }],
      })).toString('base64'),
    );
  });

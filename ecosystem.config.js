module.exports = {
  apps: [
    // {
    //   name: 'lint',
    //   script: 'npm run test:lint -- --watch',
    //   env: {
    //     DEBUG: '',
    //   },
    //   watch: false,
    // },
    // {
    //   name: 'test',
    //   script: 'npm run test:unit -- --watchAll',
    //   env: {
    //     DEBUG: '',
    //   },
    //   watch: false,
    // },
    {
      name: 'register',
      script: 'functions-framework --port=8080 --target=register',
      watch: ['src'],
    },
    {
      name: 'login_token',
      script: 'functions-framework --port=8081 --target=loginToken',
      watch: ['src'],
    },
    {
      name: 'refresh_token',
      script: 'functions-framework --port=8082 --target=refreshToken',
      watch: ['src'],
    },
    {
      name: 'guest_token',
      script: 'functions-framework --port=8083 --target=guestToken',
      watch: ['src'],
    },
    {
      name: 'member',
      script: 'functions-framework --port=8084 --target=member',
      watch: ['src'],
    },
    {
      name: 'jwks',
      script: 'functions-framework --port=8085 --target=jwks',
      watch: ['src'],
    },
    {
      name: 'submitApplication',
      script: 'functions-framework --port=8086 --target=submitApplication',
      watch: ['src'],
    },
    {
      name: 'pay',
      script: 'functions-framework --port=8087 --target=pay',
      watch: ['src'],
    },
    {
      name: 'confirmEmail',
      script: 'functions-framework --port=8088 --target=confirmEmail',
      watch: ['src'],
    },
    {
      name: 'memberAdmin',
      script: 'functions-framework --port=8089 --target=memberAdmin',
      watch: ['src'],
    },
    {
      name: 'memberStateAdmin',
      script: 'functions-framework --port=8090 --target=memberStateAdmin',
      watch: ['src'],
    },
    {
      name: 'oauth2Discord',
      script: 'functions-framework --port=8091 --target=oauth2Discord',
      watch: ['src'],
    },
    {
      name: 'discordInfo',
      script: 'functions-framework --port=8092 --target=discordInfo',
      watch: ['src'],
    },
    {
      name: 'discordMember',
      script: 'functions-framework --port=8093 --target=discordMember',
      watch: ['src'],
    },
  ],
};

terraform {
  backend "gcs" {
  }
}
provider "random" {
}
provider "aws" {
  region = "ap-southeast-2"
}
provider "google" {
  region  = var.region
  project = "${var.google_project_id_prefix}-${terraform.workspace}"
}

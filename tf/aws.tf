resource "aws_iam_user" "run" {
  name = "member_profiles_${var.env}"
  path = "/api/"
}
resource "aws_iam_access_key" "run2" {
  user = aws_iam_user.run.name
}
resource "aws_iam_user_policy" "run" {
  name = "ses"
  user = aws_iam_user.run.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ses:SendEmail",
        "ses:SendRawEmail"
      ],
      "Effect": "Allow",
      "Resource": "*",
      "Condition": {
        "StringEquals": {
          "ses:FromAddress": "${local.email_from_addr}"
        }
      }
    }
  ]
}
EOF
}

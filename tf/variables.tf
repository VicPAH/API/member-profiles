variable "region" {
  default = "australia-southeast1"
}
variable "env" {
  type = string
}
variable "commit_short_sha" {
  type = string
}
variable "google_project_id_prefix" {
  type = string
}


variable "site_base_url" {
  type = string
}
variable "member_profiles_jwk_b64" {
  type      = string
  sensitive = true
}
variable "recaptcha_secret" {
  type      = string
  sensitive = true
}
variable "square_env" {
  type = string
}
variable "square_secret" {
  type      = string
  sensitive = true
}
variable "square_location_id" {
  type = string
}
variable "discord_app_id" {
  type = string
}
variable "discord_app_secret" {
  type      = string
  sensitive = true
}
variable "discord_bot_token" {
  type      = string
  sensitive = true
}
variable "discord_server_id" {
  type = string
}


locals {
  email_from_suffix = var.env == "prod" ? "" : "-${var.env}"
  email_from_addr   = "noreply${local.email_from_suffix}@vicpah.org.au"
}

variable "name" {
  type = string
}
variable "description" {
  type = string
}
variable "runtime" {
  type    = string
  default = "nodejs12"
}
variable "code_object" {
  type = object({ bucket = string, name = string })
}
variable "entry_point" {
  type = string
}
variable "environment_variables" {
  type = map(any)
}
variable "public" {
  type    = bool
  default = false
}

locals {
  package_suffix = formatdate("YYYYMMDD-hhmmss", timestamp())
}
resource "random_id" "code_bucket" {
  byte_length = 2
  prefix      = "member_profiles_code_"
}
resource "random_string" "email_confirm_salt" {
  length = 32
}
resource "google_storage_bucket" "code" {
  name = random_id.code_bucket.hex
}
resource "google_storage_bucket_object" "code" {
  name         = "package-${local.package_suffix}.zip"
  bucket       = google_storage_bucket.code.name
  source       = "../package.zip"
  content_type = "application/zip"
  metadata     = {}
}


module "register_fn" {
  source      = "./modules/gcp_function"
  name        = "register"
  entry_point = "register"
  description = "Public member registration"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64            = var.member_profiles_jwk_b64
    member_profiles_from_addr          = local.email_from_addr
    member_profiles_confirm_email_salt = random_string.email_confirm_salt.id
    AWS_ACCESS_KEY_ID                  = aws_iam_access_key.run2.id
    AWS_SECRET_ACCESS_KEY              = aws_iam_access_key.run2.secret
    AWS_REGION                         = "ap-southeast-2"
  }
}
module "confirm_email_admin_fn" {
  source      = "./modules/gcp_function"
  name        = "confirm_email_admin"
  entry_point = "confirmEmailAdmin"
  description = "Confirm a member's email address (manual run)"
  code_object = google_storage_bucket_object.code

  environment_variables = {
    member_profiles_jwk_b64            = var.member_profiles_jwk_b64
    member_profiles_from_addr          = local.email_from_addr
    member_profiles_confirm_email_salt = random_string.email_confirm_salt.id
    AWS_ACCESS_KEY_ID                  = aws_iam_access_key.run2.id
    AWS_SECRET_ACCESS_KEY              = aws_iam_access_key.run2.secret
    AWS_REGION                         = "ap-southeast-2"
    square_secret                      = var.square_secret
    square_env                         = var.square_env
    square_location_id                 = var.square_location_id
  }
}
module "confirm_email_fn" {
  source      = "./modules/gcp_function"
  name        = "confirm_email"
  entry_point = "confirmEmail"
  description = "Confirm a member's email address"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64            = var.member_profiles_jwk_b64
    member_profiles_from_addr          = local.email_from_addr
    member_profiles_confirm_email_salt = random_string.email_confirm_salt.id
    AWS_ACCESS_KEY_ID                  = aws_iam_access_key.run2.id
    AWS_SECRET_ACCESS_KEY              = aws_iam_access_key.run2.secret
    AWS_REGION                         = "ap-southeast-2"
    square_secret                      = var.square_secret
    square_env                         = var.square_env
    square_location_id                 = var.square_location_id
  }
}
module "jwks_fn" {
  source      = "./modules/gcp_function"
  name        = "jwks"
  entry_point = "jwks"
  description = "Get the JWKS for token signing"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
  }
}
module "member_fn" {
  source      = "./modules/gcp_function"
  name        = "member"
  entry_point = "member"
  description = "Get a member by ID"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
    square_secret           = var.square_secret
    square_env              = var.square_env
    square_location_id      = var.square_location_id
  }
}
module "members_admin_fn" {
  source      = "./modules/gcp_function"
  name        = "member_admin"
  entry_point = "memberAdmin"
  description = "Members admin list/create"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
  }
}
module "members_state_admin_fn" {
  source      = "./modules/gcp_function"
  name        = "member_state_admin"
  entry_point = "memberStateAdmin"
  description = "Members state log admin"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
  }
}
module "submit_application_fn" {
  source      = "./modules/gcp_function"
  name        = "submit_application"
  entry_point = "submitApplication"
  description = "Submit an application for membership"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
  }
}
module "pay_fn" {
  source      = "./modules/gcp_function"
  name        = "pay"
  entry_point = "pay"
  description = "Pay membership dues"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
    square_secret           = var.square_secret
    square_env              = var.square_env
    square_location_id      = var.square_location_id
  }
}
module "guest_token_fn" {
  source      = "./modules/gcp_function"
  name        = "guest_token"
  entry_point = "guestToken"
  description = "Create a guest token from a reCAPTCHA token"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
    recaptcha_secret        = var.recaptcha_secret
  }
}
module "login_token_fn" {
  source      = "./modules/gcp_function"
  name        = "login_token"
  entry_point = "loginToken"
  description = "Create a login token from username/password"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
  }
}
module "refresh_token_fn" {
  source      = "./modules/gcp_function"
  name        = "refresh_token"
  entry_point = "refreshToken"
  description = "Create a refreshed token from a login token"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
  }
}
module "oauth2_discord_fn" {
  source      = "./modules/gcp_function"
  name        = "oauth2_discord"
  entry_point = "oauth2Discord"
  description = "Login/link with Discord"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
    site_base_url           = var.site_base_url
    discord_app_id          = var.discord_app_id
    discord_app_secret      = var.discord_app_secret
  }
}
module "discord_info_fn" {
  source      = "./modules/gcp_function"
  name        = "discord_info"
  entry_point = "discordInfo"
  description = "Server info from Discord"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    discord_bot_token = var.discord_bot_token
    discord_server_id = var.discord_server_id
  }
}
module "discord_member_fn" {
  source      = "./modules/gcp_function"
  name        = "discord_member"
  entry_point = "discordMember"
  description = "Member info from Discord"
  code_object = google_storage_bucket_object.code
  public      = true

  environment_variables = {
    member_profiles_jwk_b64 = var.member_profiles_jwk_b64
    discord_bot_token       = var.discord_bot_token
    discord_server_id       = var.discord_server_id
  }
}

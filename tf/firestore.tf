resource "google_app_engine_application" "app" {
  location_id   = var.region
  database_type = "CLOUD_FIRESTORE"
  depends_on = [
    google_project_service.appengine,
    google_project_service.firestore,
  ]
}

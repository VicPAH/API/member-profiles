const debug = require('debug');

const { BadRequestError, ForbiddenError, InternalServerError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const { applyMemberStates, dbFullSchema, fromFirestorePre, httpReadSchema } = require('../models/member');

const { refreshAud } = require('./token');

const LOG = debug('member');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

exports.submitApplication = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(refreshAud)),

  async (req, res, next) => {
    const id = req.auth.getId();
    if (!req.url.endsWith(`/${id}`)) {
      LOG('ForbiddenError');
      throw new ForbiddenError();
    }

    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef } = req.di;
    const db = getDb();
    const membersRef = getMembersRef().withConverter({
      toFirestore: createToFirestore(dbFullSchema),
      fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
    });
    const docRef = membersRef.doc(id);

    LOG_TRACE('Starting TX');
    await db.runTransaction(async tx => {
      let memberData = (await tx.get(docRef)).data();
      if (!memberData.emailVerified) {
        throw new BadRequestError([{
          message: "Can't submit application without email verification",
          path: ['emailVerified'],
          type: 'any.only',
        }]);
      }

      memberData = applyMemberStates(memberData);
      if (memberData.membershipSvc.state.nextEvents.indexOf('SUBMIT_APPLICATION') < 0) {
        throw new BadRequestError([{
          message: "Can't submit application in this state",
          path: ['stateLog'],
          type: 'any.state',
        }]);
      }

      memberData.stateLog.push({
        action: 'SUBMIT_APPLICATION',
        date: new Date(),
        note: 'User initiated',
      });
      memberData = applyMemberStates(memberData);

      const { error: validateError } = dbFullSchema.tailor('member').validate(memberData, { allowUnknown: true });
      if (validateError) throw new BadRequestError(validateError.details);

      await tx.update(docRef, {
        stateLog: memberData.stateLog,
      });
    });

    LOG_TRACE('TX done; serializing return');
    const {
      error: finalError,
      value: finalValue,
    } = httpReadSchema.validate({
      ...(await docRef.get()).data(),
      id,
    }, { stripUnknown: true });
    if (finalError) {
      LOG_ERR('Error serializing return:', finalError);
      throw new InternalServerError();
    }

    LOG_TRACE('Finalizing and sending response:', finalValue);
    res.set('Content-Location', `/member/${finalValue.id}`);
    res.status(200).json(finalValue);
  },
);

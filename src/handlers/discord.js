// const debug = require('debug');
const _ = require('lodash');

const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { UnauthorizedError, BadRequestError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');

const { refreshAud } = require('./token');

// const LOG = debug('discord');
// const LOG_ERROR = LOG.extend('error');
// const LOG_TRACE = LOG.extend('trace');

const assignableRoles = [
  '895974315047141408', // dev - pup
  '895974386467766283', // dev - handler
  '895974407049195540', // dev - pandler
  '895974431879495731', // dev - he him
  '895974455774425118', // dev - she her
  '895974481535856670', // dev - they them
  '896259618194485258', // dev - zi zir
  '896023567265857616', // dev - orange
  '896023895344312350', // dev - green

  '751063421449142302', // prod - pup
  '751063466323869779', // prod - handler
  '751063589674287115', // prod - pandler
  '785004427819089920', // prod - kitty
  '779642608342663209', // prod - he him
  '779642591389679626', // prod - she her
  '779642622284922890', // prod - they them
  '779650753051623444', // prod - zi zir
  '901764805818458125', // prod - it its
  '896205720838610954', // prod - red
  '896206763198648383', // prod - orange
  '896206815556173874', // prod - yellow
  '896207011191087145', // prod - green
  '896207183438557194', // prod - blue
  '896207335066853377', // prod - purple
  '896207412787294279', // prod - pink
  '896207495368953886', // prod - black
  '896950829356027924', // prod - white
];

exports.discordInfo = chainMiddleware(
  ...defaultPreflights(['GET']),

  async (req, res) => {
    const client = req.di.getDiscordBotClient();
    const discRes = await client.get(`/guilds/${req.di.getDiscordServerId()}`);
    res.status(200).json(_.pick(discRes.data, [
      'id', 'name', 'roles',
    ]));
    res.end();
  },
);

const discordMemberRead = async (req, res, discordId) => {
  const client = req.di.getDiscordBotClient();
  const [memberRes, rolesRes] = await Promise.all([
    client.get(`/guilds/${req.di.getDiscordServerId()}/members/${discordId}`),
    client.get(`/guilds/${req.di.getDiscordServerId()}/roles`),
  ]);

  res.status(200).json({
    ..._.pick(memberRes.data, [
      'user', 'nick', 'avatar',
    ]),
    roles: memberRes.data.roles.map(roleId => rolesRes.data.find(role => role.id === roleId)),
  });
  res.end();
};

exports.discordMember = chainMiddleware(
  ...defaultPreflights(['GET', 'POST']),
  authenticate(jwtProvider(refreshAud)),

  async (req, res) => {
    const authModel = await req.auth.getModel();
    const authData = authModel.data();
    if (!authData.discordId) throw new UnauthorizedError('You have not linked your Discord account');

    if (req.method === 'POST') {
      const serverId = req.di.getDiscordServerId();
      const memberId = authData.discordId;

      for (const id of req.body.roles || []) {
        if (assignableRoles.indexOf(id) < 0) throw new BadRequestError(`Not allowed to assign role ${id}`);
      }
      const client = req.di.getDiscordBotClient();
      const memberRes = await client.get(`/guilds/${serverId}/members/${memberId}`);

      for (const bodyId of req.body.roles) {
        if (!memberRes.data.roles.find(reqId => reqId === bodyId)) await client.put(`/guilds/${serverId}/members/${memberId}/roles/${bodyId}`);
      }
      for (const reqId of memberRes.data.roles) {
        if (assignableRoles.indexOf(reqId) < 0) continue;
        if (!req.body.roles.find(bodyId => reqId === bodyId)) await client.delete(`/guilds/${serverId}/members/${memberId}/roles/${reqId}`);
      }
    }

    await discordMemberRead(req, res, authData.discordId);
  },
);

const debug = require('debug');
const _ = require('lodash');

const { BadRequestError, ForbiddenError, InternalServerError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const {
  applyMemberStates,

  dbFullSchema,
  httpReadSchema,

  fromFirestorePre,
} = require('../models/member');
const {
  httpPostSchema,
  squareMetaReportSchema,
  squareMetaDbSchema,
} = require('../models/pay');
const square = require('../square');

const { refreshAud } = require('./token');

const LOG = debug('member');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

const RATES = {
  full: 3500,
  concession: 2000,
};

exports.pay = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(refreshAud)),

  async (req, res, next) => {
    const id = req.auth.getId();
    if (!req.url.endsWith(`/${id}`)) {
      LOG('ForbiddenError');
      throw new ForbiddenError();
    }

    const {
      error: bodyError,
      value: bodyValue,
    } = httpPostSchema.validate(req.body);
    if (bodyError) throw new BadRequestError(bodyError.details);

    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef, sendEmail } = req.di;
    const db = getDb();
    const membersRef = getMembersRef().withConverter({
      toFirestore: createToFirestore(dbFullSchema),
      fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
    });

    const docRef = membersRef.doc(id);
    LOG_TRACE('Starting TX');
    await db.runTransaction(async tx => {
      let data = (await tx.get(docRef)).data();

      data = applyMemberStates(data);
      if (data.membershipSvc.state.nextEvents.indexOf('PAY') < 0) {
        throw new BadRequestError([{
          message: 'Membership fees not due',
          path: ['stateLog'],
          type: 'any.state',
        }]);
      }

      const client = square.getClient();
      const squareResp = await client.paymentsApi.createPayment({
        sourceId: bodyValue.nonce,
        verificationToken: bodyValue.token,
        idempotencyKey: bodyValue.key,
        autocomplete: true,
        customerId: await square.getMember({ ...data, id }).id,
        buyerEmailAddress: data.email,
        amountMoney: {
          amount: RATES[bodyValue.rate],
          currency: 'AUD',
        },
        note: `${new Date().getFullYear()} membership payment for ${id}`,
      });

      const paymentStatus = _.get(squareResp, 'result.payment.status');
      if (paymentStatus === 'COMPLETED') {
        const {
          error: squareMetaError,
          value: squareMeta,
        } = squareMetaDbSchema.validate(squareResp.result, { stripUnknown: true });
        if (squareMetaError) LOG_ERR('Error processing square meta:', squareMetaError);

        data.stateLog.push({
          action: 'PAY',
          date: new Date(),
          note: 'Paid via VicPAH website',
          meta: {
            ...squareMeta,
            type: 'square',
          },
        });
        data = applyMemberStates(data);
        data.stateLog = data.stateLog.filter(logEntry => logEntry.action !== 'BEGIN_MEMBERSHIP_PERIOD');
        await tx.update(docRef, { stateLog: data.stateLog });
      } else {
        const {
          error: squareMetaError,
          value: squareMeta,
        } = squareMetaReportSchema.validate(squareResp.result, { stripUnknown: true });
        if (squareMetaError) LOG_ERR('Error processing square meta:', squareMetaError);
        const memberMeta = _.pick(data, ['id', 'displayName', 'email', 'givenName']);

        await sendEmail({
          subject: 'Failed membership payment',
          toAddr: 'infra@vicpah.org.au',
          bodyText: `Membership payment for ${id} failed.\n\nMEMBER:\n${JSON.stringify(memberMeta)}\n\nSQUARE:\n${JSON.stringify(squareMeta)}`,
        }, req.di);
        if (_.isString(paymentStatus)) {
          throw new BadRequestError([{
            message: `Card payment ${paymentStatus.toLowerCase()}`,
            path: [],
            type: 'any.custom',
          }]);
        } else {
          throw new InternalServerError();
        }
      }
    });

    const memberData = (await docRef.get()).data();

    LOG_TRACE('TX done; serializing return');
    const {
      error: finalError,
      value: finalValue,
    } = httpReadSchema.validate({
      ...memberData,
      id,
    }, { stripUnknown: true });
    if (finalError) {
      LOG_ERR('Error serializing return:', finalError);
      throw new InternalServerError();
    }

    LOG_TRACE('Finalizing and sending response:', finalValue);
    res.set('Content-Location', `/member/${finalValue.id}`);
    res.status(200).json(finalValue);
  },
);

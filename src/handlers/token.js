const debug = require('debug');
const { v4: uuidv4 } = require('uuid');

const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { authenticate, basicProvider, jwtProvider, recaptchaProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');

const LOG = debug('token');
const LOG_ERROR = LOG.extend('error');
const LOG_TRACE = LOG.extend('trace');

const issuer = 'vicpah.org.au:member_profiles';
const refreshAud = 'vicpah.org.au';
const loginAud = `${refreshAud}/refresh_token`;
const guestAud = `${refreshAud}/guest`;
Object.assign(exports, { issuer, refreshAud, loginAud, guestAud });

exports.guestToken = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(recaptchaProvider),

  async (req, res) => {
    LOG_TRACE('Generating JWT opts');
    const jwtOpts = {
      issuer,
      audience: [guestAud],
      expiresIn: '24 hours',
      subject: `guest.${uuidv4()}`,
    };
    LOG_TRACE('Creating JWT:', jwtOpts);
    try {
      const token = await req.di.createJwt({}, jwtOpts);
      res.status(201).json({ token });
    } catch (err) {
      LOG_ERROR('Error generating token:', err);
      throw err;
    }
  },
);
exports.sendLoginToken = async (req, res) => {
  LOG_TRACE('Generating JWT opts');
  const jwtOpts = {
    issuer,
    audience: [loginAud],
    subject: `${req.auth.getId()}`,
  };
  LOG_TRACE('Creating JWT:', jwtOpts);
  try {
    const token = await req.di.createJwt({}, jwtOpts);
    res.status(201).json({ token });
  } catch (err) {
    LOG_ERROR('Error generating token:', err);
    throw err;
  }
};
exports.loginToken = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(basicProvider),
  exports.sendLoginToken,
);
exports.refreshToken = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(loginAud)),

  async (req, res) => {
    LOG_TRACE('Generating JWT opts');
    const id = req.auth.getId();
    const ref = req.di.getMembersRef().doc(id);
    const doc = await ref.get();
    const jwtOpts = {
      issuer,
      audience: [refreshAud],
      subject: `${req.auth.getId()}`,
      expiresIn: '5 minutes',
    };
    LOG_TRACE('Creating JWT:', jwtOpts);
    try {
      const token = await req.di.createJwt({
        scope: (doc.data().scope || []).join(' '),
      }, jwtOpts);
      res.status(201).json({ token });
    } catch (err) {
      LOG_ERROR('Error generating token:', err);
      throw err;
    }
  },
);

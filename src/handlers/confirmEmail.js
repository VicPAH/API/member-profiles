const debug = require('debug');
const _ = require('lodash');

const { BadRequestError, ForbiddenError, NotFoundError, UnauthorizedError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const square = require('../square');

const {
  dbFullSchema,

  fromFirestorePre,
} = require('../models/member');

const { refreshAud } = require('./token');

const LOG = debug('confirmEmail');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

exports.confirmEmailAdmin = async (req, res) => {
  const { reqId, authId, code, resend } = req.body;
  if (authId && reqId !== authId) {
    LOG('ForbiddenError');
    throw new ForbiddenError();
  }

  LOG_TRACE('Getting DI');
  const { getDb, getMembersRef, sendConfirmEmail } = req.di;
  const db = getDb();
  const membersRef = getMembersRef().withConverter({
    fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
  });

  if (resend) {
    LOG_TRACE('Starting resend');
    if (reqId !== authId) {
      LOG('ForbiddenError');
      throw new ForbiddenError();
    }

    LOG_TRACE('Getting doc');
    const docRef = membersRef.doc(reqId);
    const doc = await docRef.get();
    if (doc.exists) {
      await sendConfirmEmail({
        member: { id: reqId, ...doc.data() },
        log: LOG_TRACE,
      }, req.di);
      res.status(202).json({ success: true });
    } else {
      throw new NotFoundError();
    }
  } else {
    LOG_TRACE('Getting true code');
    const trueCode = req.di.getConfirmEmailCode(reqId, req.di);

    if (code.trim() === trueCode) {
      LOG_TRACE('Code correct; writing');
      const docRef = membersRef.doc(reqId);
      try {
        LOG_TRACE('Starting TX');
        await db.runTransaction(async tx => {
          const docData = (await tx.get(docRef)).data();
          await Promise.all([
            tx.update(docRef, { emailVerified: true }),
            square.ensureMember({ ...docData, id: reqId }),
          ]);
        });
        res.status(200).json({ success: true });
      } catch (err) {
        // TODO yeeeeah this isn't always 404
        LOG_ERR('Error writing confirmation:', err);
        throw new NotFoundError();
      }
    } else {
      LOG_TRACE('Code incorrect');
      throw new BadRequestError([{
        message: 'Code is incorrect',
        path: ['code'],
        type: 'any.only',
      }]);
    }
  }
};

exports.confirmEmail = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(refreshAud)),

  async (req, res, next) => {
    const pathParts = req.url.split('/');
    const lastPart = pathParts[pathParts.length - 1];

    if (!req.auth) throw new UnauthorizedError();

    if (lastPart === 'resend') {
      return exports.confirmEmailAdmin(
        {
          ..._.pick(req, ['di']),
          body: {
            authId: req.auth.getId(),
            reqId: pathParts[pathParts.length - 2],
            resend: true,
          },
        },
        res,
      );
    }
    return exports.confirmEmailAdmin(
      {
        ..._.pick(req, ['di']),
        body: {
          authId: req.auth.getId(),
          reqId: lastPart,
          resend: false,
          code: req.body.code,
        },
      },
      res,
    );
  },
);

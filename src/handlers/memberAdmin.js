const debug = require('debug');
const _ = require('lodash');

const { BadRequestError, ForbiddenError, NotFoundError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const {
  applyMemberStates,

  dbFullSchema,
  httpReadAdminSchema,
  httpReadCommitteeSchema,

  fromFirestorePre,
} = require('../models/member');
const { actionSchemas } = require('../models/memberAdmin');

const { refreshAud } = require('./token');

const LOG = debug('member');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

exports.memberAdmin = chainMiddleware(
  ...defaultPreflights(['GET', 'POST']),
  authenticate(jwtProvider(refreshAud)),

  async (req, res, next) => {
    const scopeRaw = req.auth.payload.scope || '';
    const scope = scopeRaw.split(' ');

    const isCommittee = scope.indexOf('committee') >= 0;
    const isSecretary = scope.indexOf('secretary') >= 0;
    if (!isCommittee) {
      throw new ForbiddenError();
    }

    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef } = req.di;
    const db = await getDb();
    const membersRef = getMembersRef().withConverter({
      toFirestore: createToFirestore(dbFullSchema),
      fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
    });

    if (req.method === 'POST') {
      if (scope.indexOf('secretary') < 0) {
        throw new ForbiddenError();
      }
      const schema = actionSchemas[req.body.action];
      const { error: bodyError, value } = schema.validate(req.body);
      if (bodyError) throw new BadRequestError(bodyError.details);

      if (value.action === 'create') {
        const doc = membersRef.doc();
        doc.create({ hideMemberList: true });
        res.status(201).location(`/members/${doc.id}`).json({ id: doc.id });
      } else if (value.action === 'merge') {
        const primaryRef = membersRef.doc(value.primaryId);
        const deleteRef = membersRef.doc(value.deleteId);
        await db.runTransaction(async tx => {
          const [primaryDoc, deleteDoc] = await Promise.all([
            tx.get(primaryRef),
            tx.get(deleteRef),
          ]);
          if (!(primaryDoc.exists && deleteDoc.exists)) {
            throw new NotFoundError();
          }

          const primaryData = primaryDoc.data();
          const deleteData = deleteDoc.data();

          if (primaryData.stateLog) {
            throw new BadRequestError([{
              message: 'Primary has state log',
              path: ['primaryId'],
              type: 'any.state',
            }]);
          }
          if (deleteData.passwordHash) {
            throw new BadRequestError([{
              message: 'Delete has password set',
              path: ['deleteId'],
              type: 'any.state',
            }]);
          }

          const newData = {
            ...deleteData,
            ..._.omitBy(primaryData, _.isNil),
          };

          await tx.update(primaryRef, newData);
          return tx.delete(deleteRef);
        });
        res.status(200).end();
      } else {
        res.status(501).end();
      }
    } else if (req.method === 'GET') {
      const snap = await membersRef.get();
      const value = [];
      for (const doc of snap.docs) {
        let preStateValue = { id: doc.id, ...doc.data() };
        preStateValue = applyMemberStates(preStateValue);
        let outputSchema;
        if (isSecretary) {
          outputSchema = httpReadAdminSchema;
        } else if (isCommittee) {
          outputSchema = httpReadCommitteeSchema;
        }
        const {
          error: docError,
          value: docValue,
        } = outputSchema.validate(preStateValue, { stripUnknown: true });
        if (docError) {
          LOG_ERR('Error serializing doc:', docError);
        }
        value.push(docValue);
      }
      res.status(200).json(value);
    } else {
      res.status(405).end();
    }
  },
);

const debug = require('debug');
const { firestore: { FieldValue } } = require('firebase-admin');
const _ = require('lodash');

const { BadRequestError, ForbiddenError, InternalServerError, NotFoundError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const {
  applyMemberStates,
  applyPasswordHash,

  dbFullSchema,
  httpReadSchema,
  httpReadAdminSchema,
  httpReadCommitteeSchema,
  httpUpdateSchema,
  httpUpdateAdminSchema,

  byEmail,

  fromFirestorePre,
} = require('../models/member');
const square = require('../square');

const { refreshAud } = require('./token');

const LOG = debug('member');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

const renameField = (data, oldName, newName) => {
  if (!data[newName] && data[oldName]) {
    data[newName] = data[oldName];
    delete data[oldName];
  }
};

exports.member = chainMiddleware(
  ...defaultPreflights(['GET', 'PATCH']),
  authenticate(jwtProvider(refreshAud)),

  async (req, res, next) => {
    const urlParts = req.url.split('/');
    const id = urlParts[urlParts.length - 1];

    const scopeRaw = req.auth.payload.scope || '';
    const scope = scopeRaw.split(' ');

    const isOwner = id === req.auth.getId();
    const isCommittee = scope.indexOf('committee') >= 0;
    const isSecretary = scope.indexOf('secretary') >= 0;

    if (!(isOwner || isCommittee)) {
      LOG('ForbiddenError');
      throw new ForbiddenError();
    }

    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef } = req.di;
    const db = getDb();
    const membersRef = getMembersRef().withConverter({
      toFirestore: createToFirestore(dbFullSchema),
      fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
    });

    const docRef = membersRef.doc(id);
    const doc = await docRef.get();
    if (!doc.exists) {
      throw new NotFoundError();
    }

    if (req.method === 'PATCH') {
      if (!(isOwner || isSecretary)) {
        LOG('ForbiddenError');
        throw new ForbiddenError();
      }
      let patchSchema = isSecretary ? httpUpdateAdminSchema : httpUpdateSchema;
      const {
        error: preValidateError,
        value: preValidateValue,
      } = patchSchema.validate(req.body);
      if (preValidateError) throw new BadRequestError(preValidateError.details);

      let value;
      if (preValidateValue.membershipCurrent || preValidateValue.membershipPayable) {
        patchSchema = patchSchema.tailor('member');
        const {
          error: validateError,
          value: validateValue,
        } = patchSchema.validate(req.body);
        if (validateError) throw new BadRequestError(validateError.details);
        value = validateValue;
      } else {
        value = preValidateValue;
      }

      if (value.discordId === null) value.discordOauth = null;

      LOG_TRACE('Starting TX');
      await db.runTransaction(async tx => {
        if (value.email) {
          LOG_TRACE('Checking email');
          const exist = await byEmail(value.email, tx, req.di);
          if (exist && exist.id !== id) {
            throw new BadRequestError([{
              message: 'Member exists with that email',
              path: ['email'],
              type: 'any.exists',
            }]);
          }
        }

        const existData = (await docRef.get()).data();

        renameField(existData, 'mobilePhone', 'mobileNumber');
        renameField(existData, 'postalAddress', 'address');
        delete existData.legalName;

        const { emailVerified } = existData;
        if (!emailVerified && !isSecretary) {
          patchSchema = patchSchema.tailor('emailUnverified');
          const {
            error: emailValidateError,
            value: emailValidateValue,
          } = patchSchema.validate(req.body);
          if (emailValidateError) throw new BadRequestError(emailValidateError.details);
          value = emailValidateValue;
        }

        value.mobilePhone = FieldValue.delete();
        value.postalAddress = FieldValue.delete();
        value.legalName = FieldValue.delete();

        LOG_TRACE('Updating existing member');
        await Promise.all([
          tx.update(
            docRef,
            _.omit(
              value.password ? applyPasswordHash(value) : value,
              ['id'],
            ),
          ),
          emailVerified ? square.ensureMember({ ...existData, ...value, id }) : null,
        ]);
      });
    }
    const memberData = (await docRef.get()).data();

    LOG_TRACE('TX done; serializing return');
    let outputSchema;
    if (isSecretary) {
      outputSchema = httpReadAdminSchema;
    } else if (isOwner) {
      outputSchema = httpReadSchema;
    } else if (isCommittee) {
      outputSchema = httpReadCommitteeSchema;
    }
    if (!memberData.emailVerified && !isSecretary) {
      outputSchema = outputSchema.tailor('emailUnverified');
    }

    let {
      error: preStateError, // eslint-disable-line prefer-const
      value: preStateValue,
    } = outputSchema.validate({
      ...memberData,
      id,
    }, { stripUnknown: true });
    if (preStateError) {
      LOG_ERR('Error serializing pre-state return:', preStateError);
      throw new InternalServerError();
    }

    preStateValue = applyMemberStates(preStateValue);

    const {
      error: finalError,
      value: finalValue,
    } = outputSchema.validate(preStateValue, { stripUnknown: true });
    if (finalError) {
      LOG_ERR('Error serializing final return:', finalError);
    }

    LOG_TRACE('Finalizing and sending response:', finalValue, memberData);
    res.set('Content-Location', `/member/${finalValue.id}`);
    res.status(200).json(finalValue);
  },
);

const axios = require('axios');
const debug = require('debug');
const _ = require('lodash');
const { AuthorizationCode } = require('simple-oauth2');

const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');
const { ForbiddenError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const { InternalServerError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider, RegisteredUser } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const {
  dbFullSchema,

  byEmail,
  byDiscordId,

  fromFirestorePre,
  dbUpdateSchema,
} = require('../models/member');

const { refreshAud, sendLoginToken } = require('./token');

const LOG = debug('oauth');
const LOG_TRACE = LOG.extend('trace');

const oauthForGuest = async ({ req, accessToken, discordUser, tx }) => {
  LOG_TRACE('Linking for guest');
  LOG_TRACE('Getting by Discord ID');
  let exist = await byDiscordId(discordUser.id, tx, req.di);
  if (exist) {
    return { action: 'login', model: exist };
  }

  LOG_TRACE('Getting by email');
  exist = await byEmail(discordUser.email, tx, req.di);
  if (exist) {
    LOG_TRACE('Account exists; Deferred error');
    return {
      action: 'deferredError',
      error: new ForbiddenError(
        'An account with that email address already exists. To login with an existing account, you must link your account from your profile first.',
      ),
    };
  }

  const { getMembersRef } = req.di;
  const membersRef = getMembersRef().withConverter({
    toFirestore: createToFirestore(dbFullSchema),
    fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
  });
  const docRef = membersRef.doc();
  await tx.create(docRef, {
    displayName: discordUser.username,
    email: discordUser.email,
    emailVerified: discordUser.verified,
    discordId: discordUser.id,
    discordOauth: _.pick(
      accessToken.token,
      ['access_token', 'refresh_token', 'scope', 'token_type', 'expires_at'],
    ),
    hideMemberList: true,
    joinDate: new Date(),
  });
  return { action: 'login', id: docRef.id };
};
const oauthForUser = async ({ req, accessToken, discordUser, tx }) => {
  LOG_TRACE('Linking for user');
  LOG_TRACE('Getting by Discord ID');
  let exist = await byDiscordId(discordUser.id, tx, req.di);
  if (exist) {
    throw new ForbiddenError('Discord account already linked to another user.');
  }

  exist = await req.auth.getModel(req.di, { toFirestoreSchema: dbUpdateSchema });
  await tx.update(
    exist.ref,
    {
      discordId: discordUser.id,
      discordOauth: _.pick(
        accessToken.token,
        ['access_token', 'refresh_token', 'scope', 'token_type', 'expires_at'],
      ),
    },
  );
  return { action: 'done', message: 'Discord account linked.' };
};

exports.oauth2Discord = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(refreshAud), false),
  async (req, res, next) => {
    const client = new AuthorizationCode({
      client: {
        id: req.di.getDiscordAppId(),
        secret: req.di.getDiscordAppSecret(),
      },
      auth: {
        tokenHost: 'https://discord.com',
        tokenPath: '/api/oauth2/token',
        revokePath: '/api/oauth2/token/revoke',
      },
    });
    const accessToken = await client.getToken({
      code: req.body.code,
      redirect_uri: `${req.di.getSiteBaseUrl()}/oauth/discord/callback`,
    });
    const resp = await axios.get(
      'https://discord.com/api/users/@me',
      {
        headers: {
          Authorization: `Bearer ${accessToken.token.access_token}`,
        },
      },
    );
    const discordUser = resp.data;

    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef } = req.di;
    const db = await getDb();

    let deferredError = new InternalServerError('Timeout');
    let deferredTimer;
    await Promise.race([
      new Promise((resolve, reject) => {
        deferredTimer = setTimeout(() => {
          LOG_TRACE('Sending deferred error');
          reject(deferredError);
        }, 3000);
      }),
      (async () => {
        const infinitePromise = new Promise(() => {});
        LOG_TRACE('Starting TX');
        const todo = await db.runTransaction(tx => (req.auth
          ? oauthForUser({ req, accessToken, discordUser, tx })
          : oauthForGuest({ req, accessToken, discordUser, tx })));
        switch (todo.action) {
          case 'deferredError':
            deferredError = todo.error;
            await infinitePromise;
            return;
          case 'login':
            if (todo.id && !todo.model) {
              const membersRef = getMembersRef().withConverter({
                toFirestore: createToFirestore(dbFullSchema),
                fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
              });
              todo.model = await membersRef.doc(todo.id).get();
            }
            clearTimeout(deferredTimer);
            req.auth = new RegisteredUser({ model: todo.model, di: req.di });
            sendLoginToken(req, res);
            return;
          case 'done':
            clearTimeout(deferredTimer);
            res.status(200).json(_.omit(todo, ['action']));
            return;
          default:
            await infinitePromise;
            break;
        }
      })(),
    ]);
  },
);

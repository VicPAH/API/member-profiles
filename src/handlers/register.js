const debug = require('debug');

const { BadRequestError, InternalServerError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { validateBody } = require('@VicPAH/gcloud-functions-joi-middleware');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, guestAud, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const {
  applyPasswordHash,

  dbFullSchema,
  httpReadSchema,
  httpRegisterSchema,

  byEmail,

  fromFirestorePre,
} = require('../models/member');

const LOG = debug('register');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

exports.register = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(guestAud)),
  validateBody(httpRegisterSchema.tailor('emailUnverified').required()),

  async (req, res, next) => {
    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef, sendConfirmEmail } = req.di;
    const db = getDb();
    const membersRef = getMembersRef().withConverter({
      toFirestore: createToFirestore(dbFullSchema),
      fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
    });

    LOG_TRACE('Starting TX');
    await db.runTransaction(async tx => {
      LOG_TRACE('Checking email');
      const exist = await byEmail(req.body.email, tx, req.di);
      if (exist && exist.passwordHash) {
        throw new BadRequestError([{
          message: 'Member exists with that email',
          path: ['email'],
          type: 'any.exists',
        }]);
      }

      if (exist) {
        LOG_TRACE('Writing existing member password');
        await tx.update(
          membersRef.doc(exist.id),
          applyPasswordHash({ ...req.body, joinDate: new Date() }),
        );
      } else {
        LOG_TRACE('Writing new member');
        const newData = applyPasswordHash({
          ...req.body,
          joinDate: new Date(),
        });
        await tx.create(membersRef.doc(), newData);
      }
    });

    LOG_TRACE('Getting new member');
    const member = await byEmail(
      req.body.email,
      null,
      { ...req.di, getMembersRef: () => membersRef },
    );
    if (!member) {
      LOG_ERR("Member doesn't look like it's been written");
      throw new InternalServerError();
    }

    LOG_TRACE('TX done; serializing return');
    const {
      error: finalError,
      value: finalValue,
    } = httpReadSchema.tailor('emailUnverified').validate({
      ...member.data(),
      id: member.id,
    }, { stripUnknown: true });
    if (finalError) {
      LOG_ERR('Error serializing return:', finalError);
      throw new InternalServerError();
    }

    try {
      await sendConfirmEmail({ member: finalValue, log: LOG_TRACE }, req.di);
    } catch (err) {
      LOG_ERR('Error sending email confirm:', err);
    }

    LOG_TRACE('Finalizing and sending response:', finalValue);
    res.set('Content-Location', `/member/${finalValue.id}`);
    res.status(201).json(finalValue);
  },
);

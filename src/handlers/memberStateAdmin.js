const debug = require('debug');

const { BadRequestError, ForbiddenError, NotFoundError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { validateBody } = require('@VicPAH/gcloud-functions-joi-middleware');
const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const { authenticate, jwtProvider } = require('../middleware/auth');
const { defaultPreflights } = require('../middleware/preflight');
const {
  applyMemberStates,

  dbFullSchema,
  httpReadAdminSchema,
  stateLogItemSchema,

  fromFirestorePre,
} = require('../models/member');

const { refreshAud } = require('./token');

const LOG = debug('member');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

exports.memberStateAdmin = chainMiddleware(
  ...defaultPreflights(['POST']),
  authenticate(jwtProvider(refreshAud)),
  validateBody(stateLogItemSchema.required()),

  async (req, res, next) => {
    const urlParts = req.url.split('/');
    const id = urlParts[urlParts.length - 1];

    const scopeRaw = req.auth.payload.scope || '';
    const scope = scopeRaw.split(' ');
    if (scope.indexOf('secretary') < 0) {
      throw new ForbiddenError();
    }

    LOG_TRACE('Getting DB DI');
    const { getDb, getMembersRef } = req.di;
    const db = getDb();
    const membersRef = getMembersRef().withConverter({
      toFirestore: createToFirestore(dbFullSchema),
      fromFirestore: createFromFirestore(dbFullSchema, fromFirestorePre),
    });

    const doc = membersRef.doc(id);
    await db.runTransaction(async tx => {
      const snap = await tx.get(doc);
      if (!snap.exists) throw new NotFoundError();
      let data = { stateLog: [], ...snap.data(), id: snap.id };
      const origStateLog = [...data.stateLog];
      data = applyMemberStates(data);
      if (data.membershipSvc.state.nextEvents.indexOf(req.body.action) < 0) {
        throw new BadRequestError([{
          message: 'State transition is not valid',
          path: ['action'],
          type: 'any.state',
        }]);
      }
      await tx.update(doc, { stateLog: [...origStateLog, req.body] });
    });

    let memberData = {
      id: doc.id,
      ...(await doc.get()).data(),
    };
    memberData = applyMemberStates(memberData);

    const {
      error: finalError,
      value: finalValue,
    } = httpReadAdminSchema.validate(memberData, { stripUnknown: true });
    if (finalError) {
      LOG_ERR('Error serializing final return:', finalError);
    }

    LOG_TRACE('Finalizing and sending response:', finalValue, memberData);
    res.set('Content-Location', `/member/${finalValue.id}`);
    res.status(200).json(finalValue);
  },
);

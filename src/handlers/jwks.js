const { chainMiddleware } = require('@VicPAH/gcloud-functions-middleware-core');

const { defaultPreflights } = require('../middleware/preflight');

exports.jwks = chainMiddleware(
  ...defaultPreflights(['GET']),
  async (req, res, next) => {
    res.status(200).json({
      keys: [await req.di.publicJwk()],
    });
  },
);

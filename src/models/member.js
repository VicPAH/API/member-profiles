const crypto = require('crypto');

const Joi = require('joi');
const _ = require('lodash');
const moment = require('moment-timezone');

const { memberService } = require('vic-model-rules-member-fsm');

const validateAsFieldValue = ops => value => {
  if (!value || !value.methodName) {
    throw new Error(`${value} is not a FieldValue operation`);
  }
  const match = ops.some(op => value.methodName === `FieldValue.${op}`);
  if (!match) {
    throw new Error(`${value.methodName} is not an allowable operation: Must be ${ops.join(', ')}`);
  }
  return value;
};

const sharedFields = {
  displayName: Joi.string().min(3).max(31).allow(null),
  givenName: Joi.string().min(3).max(255).allow(null),
  familyName: Joi.string().min(3).max(255).allow(null),
  email: Joi.string().lowercase().email(),
  mobileNumber: Joi.string().max(255).allow(null),
  address: Joi.string().max(255).allow(null),
  birthDate: Joi
    .date()
    .greater('1920-01-01')
    .less('now')
    .iso()
    .allow(null),
  hideMemberList: Joi.boolean(),
  // petPlayRoles: Joi
  //   .array()
  //   .items(
  //     Joi
  //       .string()
  //       .allow('pet', 'handler'),
  //   )
  //   .unique()
  //   .default([]),
  // petStyle: Joi
  //   .string()
  //   .allow(null, 'fox', 'kitty', 'pup')
  //   .default(null),
  pronouns: Joi
    .string()
    .valid(null, 'he_him', 'she_her', 'they_them', 'zi_zir', 'it_its'),
};
const stateLogItemFields = {
  action: Joi.string().allow(
    'SUBMIT_APPLICATION',
    'PAY',
    'RAISE',
    'APPROVE',
    'REJECT',
    'REFUND',
    'EXPEL',
    'RESIGN',
    'BEGIN_MEMBERSHIP_PERIOD',
  ).required(),
  date: Joi.date().iso().required(),
  effectiveDate: Joi.date().iso(),
  note: Joi.string(),
  meta: Joi.any(),
};
const stateLogItemSchema = Joi.object(stateLogItemFields);
const readFields = {
  id: Joi.string().min(20).max(20).alphanum(),
  joinDate: Joi.date().iso(),
  stateLog: Joi
    .array()
    .items(stateLogItemSchema),
  scope: Joi
    .array()
    .items(
      Joi
        .string()
        .valid(
          'secretary',
          'committee',
        ),
    )
    .unique(),
  membershipCurrent: Joi.boolean(),
  membershipPayable: Joi.boolean(),
  emailVerified: Joi.boolean(),
  discordId: Joi.string().allow(null),
};
const writeFields = {
  password: Joi.string().min(5).max(255),
  discordId: Joi.string().valid(null),
};
const adminFields = {
  committeeNotes: Joi.string(),
};

const dbUpdateFields = {
  ...sharedFields,
  ...readFields,
  ...adminFields,
  passwordHash: Joi.string(),
  ..._.chain(sharedFields)
    .pick(['displayName', 'givenName', 'email', 'address'])
    .mapValues(field => field.alter({
      member: schema => schema.allow(Joi.override),
    }))
    .value(),

  mobilePhone: Joi.any().custom(validateAsFieldValue(['delete'])),
  postalAddress: Joi.any().custom(validateAsFieldValue(['delete'])),
  legalName: Joi.any().custom(validateAsFieldValue(['delete'])),
  discordId: Joi.any().custom(validateAsFieldValue(['delete'])),
  discordOauth: Joi.any().custom(validateAsFieldValue(['delete'])),
};
const dbFullFields = { // TODO add required fields
  ..._.omit(dbUpdateFields, ['mobilePhone', 'postalAddress', 'legalName', 'discordOauth']),
  ..._.chain(dbUpdateFields)
    .pick(['hideMemberList'])
    .mapValues(field => field.required())
    .value(),
  discordId: Joi.string().allow(null),
  discordOauth: Joi.object({
    access_token: Joi.string().required(),
    refresh_token: Joi.string().required(),
    expires_at: Joi.date().iso().required(),
    scope: Joi.string().required(),
    token_type: Joi.string().required(),
  }).allow(null),
};

let httpReadFields = {
  ..._.mapValues({
    ...sharedFields,
    ...readFields,
  }, (field, key) => {
    if (key === 'scope' || key === 'stateLog') {
      return field.default([]);
    }
    return field.default(null);
  }),
  emailVerified: readFields.emailVerified.default(false),
};
httpReadFields = {
  ...httpReadFields,
  ..._.chain(httpReadFields)
    .omit([
      'id',
      'email',
      'emailVerified',
    ])
    .mapValues(field => field.alter({
      emailUnverified: schema => schema.strip(),
    }))
    .value(),
};

const httpReadAdminFields = {
  ...sharedFields,
  ...readFields,
  ...adminFields,
};
const httpReadCommitteeFields = _.omit(httpReadAdminFields, [
  'email',
  'mobileNumber',
  'address',
  'birthDate',
  'discordId',
]);

// const httpMinimalFields = _.pick(httpReadFields, ['id', 'displayName']);

const httpWriteFieldsBase = {
  ...sharedFields,
  ...writeFields,
  ..._.chain(sharedFields)
    .pick(['displayName', 'givenName', 'familyName', 'mobileNumber', 'address', 'birthDate'])
    .mapValues(field => field.alter({
      emailUnverified: schema => schema.forbidden(),
    }))
    .value(),
};
const httpCreateFields = {
  ...httpWriteFieldsBase,
  hideMemberList: httpWriteFieldsBase.hideMemberList.default(false),
};

const httpRegisterFields = {
  ...httpCreateFields,
  ..._.chain(httpCreateFields)
    .pick(['password', 'email'])
    .mapValues((field, name) => field.required())
    .value(),
};

const httpCreateAdminFields = {
  ...httpCreateFields,
  ...adminFields,
};

const httpUpdateFields = {
  ...httpWriteFieldsBase,
  ..._.chain(httpWriteFieldsBase)
    .pick(['displayName', 'givenName', 'email', 'address'])
    .mapValues(field => field.alter({
      member: schema => schema.allow(Joi.override),
    }))
    .value(),
};

const httpUpdateAdminFields = {
  ...httpUpdateFields,
  ...adminFields,
};

// 'id',
// 'password_hash',  // n
// 'display_name',
// 'display_pic',  // n
// 'legal_name',
// 'email',  // n
// 'mobile_phone',  // n
// 'postal_address',  // n
// 'dob',  // n
// 'join_date',
// 'hide_member_list',
// 'committee_notes',  // n

const dbFullSchema = Joi.object(dbFullFields);
const dbUpdateSchema = Joi.object(dbUpdateFields);

const httpReadSchema = Joi.object(httpReadFields);
const httpReadAdminSchema = Joi.object(httpReadAdminFields);
const httpReadCommitteeSchema = Joi.object(httpReadCommitteeFields);
// const httpMinimalSchema = Joi.array().items(httpMinimalFields);

const httpRegisterSchema = Joi.object(httpRegisterFields);
const httpCreateAdminSchema = Joi.object(httpCreateAdminFields);

const httpUpdateSchema = Joi.object(httpUpdateFields);
const httpUpdateAdminSchema = Joi.object(httpUpdateAdminFields);

const PASSWORD_HASH_NAME = 'sha256';
const PASSWORD_HASH_OPTS = {};
const _passwordHashObj = (password, hashName, opts, salt) => {
  const hash = crypto.createHash(hashName);
  hash.update(password);
  hash.update(Buffer.from([0]));
  hash.update(salt);
  return { hashName, opts, salt, passwordHash: hash.digest('base64') };
};
const _passwordHashStr = (...args) => {
  const hashObj = _passwordHashObj(...args);
  return [
    hashObj.hashName,
    JSON.stringify(hashObj.opts),
    hashObj.salt,
    hashObj.passwordHash,
  ].join(';');
};
const _parsePasswordHashStr = str => {
  const [hashName, optsStr, salt, passwordHash] = str.split(';');
  return {
    hashName,
    passwordHash,
    salt,
    opts: JSON.parse(optsStr),
  };
};
const applyPasswordHash = obj => ({
  ..._.omit(obj, ['password']),
  passwordHash: _passwordHashStr(
    obj.password,
    PASSWORD_HASH_NAME,
    PASSWORD_HASH_OPTS,
    obj.email,
  ),
});
const getLogItemDate = logItem => logItem.effectiveDate || logItem.date;
const stateLogCmp = (left, right) => {
  const lDate = getLogItemDate(left);
  const rDate = getLogItemDate(right);
  if (lDate < rDate) return -1;
  if (lDate > rDate) return 1;
  return 0;
};
const applyMemberService = obj => {
  const svc = memberService().start();
  const stateLog = [...obj.stateLog || []];
  stateLog.sort(stateLogCmp);

  if (stateLog.length > 0) {
    const nowYear = moment.tz('Australia/Melbourne').year();
    const firstYear = moment(getLogItemDate(stateLog[0]))
      .tz('Australia/Melbourne')
      .year();
    for (let offset = 1; offset < nowYear - firstYear + 2; offset++) {
      const logDate = moment.tz(`${firstYear + offset}-01-01T00:00:00`, 'Australia/Melbourne');
      stateLog.push({
        action: 'BEGIN_MEMBERSHIP_PERIOD',
        date: logDate.toDate(),
      });
    }
  }
  stateLog.sort(stateLogCmp);

  const now = new Date();
  const finalStateLog = [];
  while (stateLog.length > 0) {
    const logItem = stateLog.shift();

    if (getLogItemDate(logItem) > now) continue;

    const isMember = svc.state.matches('member') || svc.state.matches('lapsedMember');
    if (!isMember && logItem.action === 'BEGIN_MEMBERSHIP_PERIOD') continue;

    svc.send(logItem.action);
    finalStateLog.push(logItem);
  }
  return {
    ...obj,
    stateLog: finalStateLog,
    membershipSvc: svc,
  };
};
const applyMemberStates = obj => {
  const objSvc = applyMemberService(obj);
  const svc = objSvc.membershipSvc;
  return {
    ...objSvc,
    membershipCurrent: svc.state.matches('member'),
    membershipPayable: svc.state.nextEvents.indexOf('PAY') >= 0,
  };
};
const checkPasswordForDoc = (doc, password) => {
  const currHashStr = doc.get('passwordHash');
  const currHashObj = _parsePasswordHashStr(currHashStr);
  const thisHashObj = _passwordHashObj(
    password, currHashObj.hashName, currHashObj.opts, currHashObj.salt,
  );

  const match = currHashObj.passwordHash === thisHashObj.passwordHash;
  return {
    match,
    update: match && !_.isEqual(currHashObj, thisHashObj),
  };
};

const createByFunction = (field, name, transform) => async (value, tx, { getMembersRef }) => {
  const query = getMembersRef()
    .where(field, '==', transform ? transform(value) : value)
    .limit(2);
  const snap = tx
    ? await tx.get(query)
    : await query.get();
  if (snap.docs.length < 1) {
    return null;
  } if (snap.docs.length === 1) {
    return snap.docs[0];
  }
  throw new Error(`Multiple documents matched the ${name} ${value}`);
};
const byEmail = createByFunction('email', 'email', v => (v ? v.toLowerCase() : v));
const byDiscordId = createByFunction('discordId', 'Discord ID');

const fromFirestorePre = data => {
  const cp = _.cloneDeep(data);
  if (data.dob) cp.dob = data.dob.toDate();
  if (data.joinDate) cp.joinDate = data.joinDate.toDate();
  if (_.get(data, 'discordOauth.expires_at')) cp.discordOauth.expires_at = data.discordOauth.expires_at.toDate();
  if (data.stateLog) {
    cp.stateLog = data.stateLog.map(log => ({
      ...log,
      ...(
        _.chain(log)
          .pick(['date', 'effectiveDate'])
          .mapValues(o => (o ? o.toDate() : undefined))
          .value()
      ),
    }));
  }
  return cp;
};

module.exports = {
  dbFullSchema,
  dbUpdateSchema,

  httpReadSchema,
  httpReadAdminSchema,
  httpReadCommitteeSchema,
  httpRegisterSchema,
  httpCreateAdminSchema,
  httpUpdateSchema,
  httpUpdateAdminSchema,

  stateLogItemSchema,

  applyPasswordHash,
  checkPasswordForDoc,

  applyMemberStates,

  byEmail,
  byDiscordId,

  fromFirestorePre,
};

const Joi = require('joi');

const { httpUpdateSchema } = require('./member');

/* eslint-disable no-loop-func */

describe('httpUpdateSchema', () => {
  describe('.tailor("member")', () => {
    let schema;
    beforeEach(() => {
      schema = httpUpdateSchema.tailor('member');
    });
    for (const field of ['displayName', 'givenName', 'email', 'address']) {
      for (const value of ['', null]) {
        test(`doesn't allow ${JSON.stringify(value)} ${field}`, () => {
          const res = schema.validate({ [field]: value });
          expect(res)
            .toEqual(expect.objectContaining({
              error: expect.any(Joi.ValidationError),
            }));
          expect(res.error.details).toEqual(
            expect.arrayContaining([
              expect.objectContaining({
                message: expect.stringContaining(`"${field}"`),
                path: [field],
              }),
            ]),
          );
        });
      }
    }
  });
});

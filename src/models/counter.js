const getCounter = (name, di) => di.getCountersRef().doc(name);
const getNext = async (name, di) => {
  const db = di.getDb();
  const ref = getCounter(name, di);

  return db.runTransaction(async tx => {
    const doc = await tx.get(ref);
    const value = doc.exists ? doc.get('value') + 1 : 1;
    await tx.set(ref, { value });
    return value;
  });
};

module.exports = {
  getCounter,
  getNext,
};

const Joi = require('joi');
const _ = require('lodash');

const createFields = {
  action: Joi.string().required().allow('create'),
};
const mergeFields = {
  action: Joi.string().required().allow('merge'),
  primaryId: Joi.string().required(),
  deleteId: Joi.string().required(),
};

const createSchema = Joi.object(createFields);
const mergeSchema = Joi.object(mergeFields);

module.exports = {
  actionSchemas: {
    create: createSchema,
    merge: mergeSchema,
  },
};

const Joi = require('joi');
const _ = require('lodash');

const httpPostFields = {
  nonce: Joi.string().min(3).max(100).required(),
  token: Joi.string().min(3).max(100).required(),
  key: Joi.string().min(3).max(100).required(),
  rate: Joi.string().valid('full', 'concession').required(),
};
const httpPostSchema = Joi.object(httpPostFields);

const squareMetaFields = {
  payment: Joi.object({
    id: Joi.any(),
    status: Joi.any(),
    amountMoney: Joi.any(),
    totalMoney: Joi.any(),
  }),
  errors: Joi.array().items(Joi.any()),
};
const squareMetaDbSchema = Joi.object(_.pick(squareMetaFields, ['payment']));
const squareMetaReportSchema = Joi.object(squareMetaFields);

module.exports = {
  httpPostSchema,

  squareMetaDbSchema,
  squareMetaReportSchema,
};

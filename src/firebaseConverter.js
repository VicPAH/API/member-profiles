const { ValidationErrorWrapper } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const convert = (schema, data, opts, pre = d => d, post = d => d) => {
  const { error, value } = schema.validate(pre(data), opts);
  if (error) throw new ValidationErrorWrapper(error);
  return post(value);
};
exports.createToFirestore = (schema, pre, post) => data => convert(
  schema, data,
  {},
  pre, post,
);
exports.createFromFirestore = (schema, pre, post) => snap => convert(
  schema, snap.data(),
  { allowUnknown: true },
  pre, post,
);

const { SESv2 } = require('@aws-sdk/client-sesv2');
const _ = require('lodash');
const process = require('process');

const _getAwsClientOpts = () => ({
});
const _getEmailFrom = () => process.env.member_profiles_from_addr;
const _getSesClient = ({ getAwsClientOpts }) => new SESv2(getAwsClientOpts());
const sendEmail = async ({ subject, toAddr, bodyText, bodyHtml }, di) => {
  const { getEmailFrom, getSesClient } = di;
  const fromAddr = getEmailFrom(di);
  const client = getSesClient(di);
  return client.sendEmail({
    FromEmailAddress: fromAddr,
    Destination: {
      ToAddresses: _.isArray(toAddr) ? toAddr : [toAddr],
    },
    Content: {
      Simple: {
        Body: {
          ...(bodyText ? { Text: { Data: bodyText } } : {}),
          ...(bodyHtml ? { Html: { Data: bodyHtml } } : {}),
        },
        Subject: { Data: subject },
      },
    },
  });
};

module.exports = {
  getAwsClientOpts: _getAwsClientOpts,
  getEmailFrom: _getEmailFrom,
  getSesClient: _getSesClient,
  sendEmail,
};

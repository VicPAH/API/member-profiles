const Eckles = require('eckles');
const jwt = require('jsonwebtoken');

const {
  createJwt,
  // verifyJwt,
} = require('./jwt');

const createKeys = async () => {
  const jwk = await Eckles.generate({ format: 'jwk' });
  return {
    ...jwk,
    pemPriv: await Eckles.export({ jwk: jwk.private }),
    pemPub: await Eckles.export({ jwk: jwk.public }),
  };
};

// let testJwkPriv;
// let testJwkPub;
let testPemPriv;
let testPemPub;
beforeAll(async () => {
  const keys = await createKeys();
  // testJwkPriv = keys.private;
  // testJwkPub = keys.public;
  testPemPriv = keys.pemPriv;
  testPemPub = keys.pemPub;
});

const di = {
  getPemPrivate: async () => testPemPriv,
  getJwtAlgorithms: async () => ['ES256'],
};

test('createJwt()', async () => {
  const token = await createJwt(
    { test: 'payload test value' },
    { audience: 'opt audience value' },
    di,
  );
  const verify = jwt.verify(
    token,
    testPemPub,
    {
      complete: true,
      algorithms: ['ES256'],
    },
  );
  expect(verify).toEqual(expect.objectContaining({
    header: {
      alg: 'ES256',
      typ: 'JWT',
    },
    payload: {
      aud: 'opt audience value',
      test: 'payload test value',
      iat: expect.any(Number),
    },
  }));
});

const process = require('process');

exports.getRecaptchaSecret = () => process.env.recaptcha_secret;

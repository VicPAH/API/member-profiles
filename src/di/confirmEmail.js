const crypto = require('crypto');
const process = require('process');

const getConfirmEmailSalt = () => process.env.member_profiles_confirm_email_salt;
const getConfirmEmailCode = (id, di) => {
  const salt = di.getConfirmEmailSalt();
  if (!salt) throw new Error('member_profiles_confirm_email_salt is undefined');
  if (!id) throw new Error('id not given');
  const hash = crypto.createHash('sha1');
  hash.update(`${id.trim()}${salt.trim()}`);
  return hash.digest('hex').substr(0, 6);
};
const sendConfirmEmail = async ({ member, log }, di) => {
  log('Sending email confirmation');
  const confirmCode = di.getConfirmEmailCode(member.id, di);
  log('Email confirmation code:', confirmCode);
  const emailBody = `
<h1>Just 1 more thing to do to confirm your account!</h1>
<p>Enter this code on <a href="https://www.vicpah.org.au/members/${member.id}">your VicPAH profile page</a>:</p>
<h2>${confirmCode}</h2>
`;
  const resp = await di.sendEmail({
    subject: 'Confirm your VicPAH email address',
    toAddr: member.email,
    bodyHtml: emailBody.trim(),
  }, di);
  log('Sent email:', resp);
};

module.exports = {
  getConfirmEmailSalt,
  getConfirmEmailCode,
  sendConfirmEmail,
};

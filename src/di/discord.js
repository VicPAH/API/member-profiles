const axios = require('axios');

// exports.getDiscordOauthUrl = () => process.env.discord_oauth_url;
exports.getDiscordAppId = () => process.env.discord_app_id;
exports.getDiscordAppSecret = () => process.env.discord_app_secret;
exports.getDiscordBotToken = () => process.env.discord_bot_token;
exports.getDiscordBotClient = () => axios.create({
  baseURL: 'https://discord.com/api',
  headers: {
    Authorization: `Bot ${exports.getDiscordBotToken()}`,
  },
});
exports.getDiscordServerId = () => process.env.discord_server_id;

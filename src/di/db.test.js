const {
  getCountersRef,
  getMembersRef,
} = require('./db');

describe('getCountersRef', () => {
  test('path', () => {
    expect(getCountersRef().path).toEqual('counters');
  });
});
describe('getMembersRef', () => {
  test('path', () => {
    expect(getMembersRef().path).toEqual('members');
  });
});

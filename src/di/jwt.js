const jwt = require('jsonwebtoken');

const { di: libDi } = require('@VicPAH/jwt-utils');

exports.publicJwk = async () => {
  const [jwk, algs] = await Promise.all([
    libDi.getJwkPublic(),
    libDi.getJwtAlgorithms(),
  ]);
  return {
    ...jwk,
    alg: algs[0],
    use: 'sig',
  };
};
exports.verifyJwt = libDi.verifyJwt;
exports.createJwt = async (
  payload,
  options = {},
  {
    getPemPrivate = libDi.getPemPrivate,
    getJwtAlgorithms = libDi.getJwtAlgorithms,
  } = {},
) => (
  jwt.sign(
    payload,
    await getPemPrivate(),
    { ...options, algorithm: (await getJwtAlgorithms())[0] },
  )
);

/* eslint-disable global-require */
module.exports = {
  ...require('./confirmEmail'),
  ...require('./db'),
  ...require('./email'),
  ...require('./jwt'),
  ...require('./discord'),
  ...require('./recaptcha'),
  ...require('./site'),
};

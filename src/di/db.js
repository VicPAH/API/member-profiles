const admin = require('firebase-admin');
const _ = require('lodash');

const getDb = _.memoize((cfg = null) => {
  admin.initializeApp();
  return admin.firestore();
});

const getCountersRef = _.memoize(() => getDb().collection('counters'));
const getMembersRef = _.memoize(() => getDb().collection('members'));

module.exports = {
  getDb,

  getCountersRef,
  getMembersRef,
};

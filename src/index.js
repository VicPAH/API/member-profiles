const di = require('./di');
const { confirmEmail, confirmEmailAdmin } = require('./handlers/confirmEmail');
const { discordInfo, discordMember } = require('./handlers/discord');
const { jwks } = require('./handlers/jwks');
const { member } = require('./handlers/member');
const { memberAdmin } = require('./handlers/memberAdmin');
const { memberStateAdmin } = require('./handlers/memberStateAdmin');
const { oauth2Discord } = require('./handlers/oauth2Discord');
const { pay } = require('./handlers/pay');
const { register } = require('./handlers/register');
const { submitApplication } = require('./handlers/submitApplication');
const { guestToken, loginToken, refreshToken } = require('./handlers/token');

const wrap = fn => (req, res) => {
  req.di = di;
  return fn(req, res);
};

module.exports = {
  confirmEmail: wrap(confirmEmail),
  confirmEmailAdmin: wrap(confirmEmailAdmin),
  discordInfo: wrap(discordInfo),
  discordMember: wrap(discordMember),
  guestToken: wrap(guestToken),
  jwks: wrap(jwks),
  loginToken: wrap(loginToken),
  member: wrap(member),
  memberAdmin: wrap(memberAdmin),
  memberStateAdmin: wrap(memberStateAdmin),
  oauth2Discord: wrap(oauth2Discord),
  pay: wrap(pay),
  refreshToken: wrap(refreshToken),
  register: wrap(register),
  submitApplication: wrap(submitApplication),
};

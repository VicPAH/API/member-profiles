const process = require('process');

const _ = require('lodash');
const { Client, Environment } = require('square');
const { v4: uuidv4 } = require('uuid');

class NotFoundError extends Error {}

class TooManyResultsError extends Error {}

const equalityFields = [
  'givenName',
  'familyName',
  'nickname',
  'emailAddress',
  // 'phoneNumber',
];

const cmpValue = (left, right) => {
  if (_.isNil(left) && (_.isNil(right) || right === '')) return true;
  if (_.isNil(right) && (_.isNil(left) || left === '')) return true;
  return _.isEqual(left, right);
};

module.exports = {
  NotFoundError,
  TooManyResultsError,
  getClient: () => new Client({
    environment: Environment[process.env.square_env],
    accessToken: process.env.square_secret,
  }),
  getMember: async (member, client = null) => {
    if (!client) client = module.exports.getClient();
    if (!member.id) throw new Error('member object must have id property');
    const resp = await client.customersApi.searchCustomers({
      limit: 2,
      query: {
        filter: {
          referenceId: { exact: member.id },
        },
      },
    });
    if (!resp.result.customers || resp.result.customers.length === 0) throw new NotFoundError();
    if (resp.result.customers.length > 1) throw new TooManyResultsError();
    return resp.result.customers[0];
  },
  transformMember: member => ({
    referenceId: member.id,
    givenName: member.givenName || '',
    familyName: member.familyName || '',
    nickname: member.displayName || '',
    emailAddress: member.email || '',
  }),
  compareCustomer: (left, right) => {
    const leftCmp = _.pick(left, equalityFields);
    const rightCmp = _.pick(right, equalityFields);
    const allKeys = new Set([...Object.keys(leftCmp), ...Object.keys(rightCmp)]);
    for (const key of allKeys) {
      const lv = leftCmp[key];
      const rv = rightCmp[key];
      if (!cmpValue(lv, rv)) return false;
    }
    return true;
  },
  ensureMember: async (member, client = null) => {
    if (!client) client = module.exports.getClient();
    let customer;
    try {
      customer = await module.exports.getMember(member, client);
    } catch (err) {
      if (err instanceof NotFoundError) {
        const createResp = await client.customersApi.createCustomer({
          idempotencyKey: uuidv4(),
          ...module.exports.transformMember(member),
        });
        return createResp.result.customer;
      }
      throw err;
    }
    const newData = module.exports.transformMember(member);
    if (!module.exports.compareCustomer(customer, newData)) {
      const updateResp = await client.customersApi.updateCustomer(
        customer.id, newData,
      );
      return updateResp.result.customer;
    }
    return customer;
  },
};

const axios = require('axios');
const basicAuth = require('basic-auth');
const debug = require('debug');
const querystring = require('querystring');

const { HttpError, InternalServerError, UnauthorizedError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');
const { createFromFirestore, createToFirestore } = require('../firebaseConverter');
const member = require('../models/member');

const LOG = debug('middleware').extend('auth');
const LOG_TRACE = LOG.extend('trace');
const LOG_ERR = LOG.extend('error');

const USER_OR_PASS_MSG = 'Incorrect username and password combination';

class User {
  constructor({ di, payload }) {
    this._di = di;
    this.payload = Object.freeze(payload);
  }
}

class RegisteredUser extends User {
  constructor({ model, ...opts }) {
    super(opts);
    this._model = model;
    this.isGuest = false;
  }

  async getModel(
    di,
    {
      toFirestoreSchema = member.dbFullSchema,
      fromFirestoreSchema = member.dbFullSchema,
    } = {},
  ) {
    if (!di) di = this._di;
    if (!this._model) {
      const { getMembersRef } = di;
      this._model = await getMembersRef()
        .withConverter({
          toFirestore: createToFirestore(toFirestoreSchema),
          fromFirestore: createFromFirestore(fromFirestoreSchema, member.fromFirestorePre),
        })
        .doc(this.payload.sub)
        .get();
    }
    return this._model;
  }

  getId() {
    return this._model
      ? this._model.id
      : this.payload.sub;
  }
}

class GuestUser extends User {
  constructor(opts) {
    super(opts);
    this.isGuest = true;
  }

  getId() {
    return null;
  }
}
Object.assign(exports, { RegisteredUser, GuestUser });

exports.authenticate = (provider, required = true, timeout = 5000, backupTimeout = 10000) => async function authenticate(req, res, next) {
  let authTimer;
  LOG_TRACE('Setting backupTimer');
  const backupTimer = setTimeout(() => {
    LOG_TRACE('Triggered backupTimer');
    clearTimeout(authTimer);
    new InternalServerError().send(res);
  }, backupTimeout);

  let authError = new InternalServerError();
  LOG_TRACE('Setting authTimer');
  authTimer = setTimeout(() => {
    LOG_TRACE('Triggered authTimer');
    clearTimeout(backupTimer);
    authError.send(res);
  }, timeout);

  try {
    LOG_TRACE('Awaiting provider');
    req.auth = await provider(req);
    LOG_TRACE('Provider gave: %o', req.auth);
  } catch (err) {
    LOG_TRACE('Auth error');
    req.auth = null;
    if (err instanceof HttpError) {
      LOG_TRACE('Auth error is HTTP error:', err);
      authError = err;
    } else {
      LOG_ERR('Unhandled auth error:', err);
      authError = new InternalServerError();
    }
  }
  if (!req.auth && required) {
    clearTimeout(authTimer);
    clearTimeout(backupTimer);
    throw new UnauthorizedError();
  }
  if (req.auth || !required) {
    clearTimeout(authTimer);
    clearTimeout(backupTimer);
    return next();
  }

  return Promise.resolve();
};

function getToken(req) {
  const headerVal = req.get('authorization');
  if (!headerVal) throw new UnauthorizedError('No Authorization header');

  if (!headerVal.toLowerCase().startsWith('bearer ')) throw new UnauthorizedError('Must authorize with Bearer token');

  return headerVal.substr(7).trim();
}

exports.recaptchaProvider = async function recaptchaProvider(req) {
  const token = getToken(req);
  const payload = {
    secret: req.di.getRecaptchaSecret(),
    response: token,
    remoteip: req.ip,
  };
  const res = await axios.post(
    'https://www.google.com/recaptcha/api/siteverify',
    querystring.stringify(payload),
  );
  if (res.data.success) {
    return new GuestUser({ di: req.di });
  }
  LOG_ERR('reCAPTCHA error:', res.data);
  return null;
};
exports.basicProvider = async function basicProvider(req) {
  LOG_TRACE('Getting authorization header');
  const headerVal = req.get('authorization');
  if (!headerVal) {
    LOG_TRACE('No authorization header value: %o', headerVal);
    throw new UnauthorizedError('No Authorization header');
  }

  LOG_TRACE('Parsing basic auth creds');
  const creds = basicAuth.parse(headerVal);
  if (!creds) {
    LOG_TRACE('Invalid basic auth header value: %o', headerVal);
    throw new UnauthorizedError('Invalid basic auth');
  }

  LOG_TRACE('Getting DB');
  const { getDb } = req.di;
  const db = getDb();

  LOG_TRACE('Starting TX');
  return db.runTransaction(async tx => {
    LOG_TRACE('Getting member by email: %o', creds.name.toLowerCase());
    const doc = await member.byEmail(creds.name.toLowerCase(), tx, req.di);
    if (!doc) {
      LOG_TRACE('Email not found: %o', creds.name.toLowerCase());
      throw new UnauthorizedError(USER_OR_PASS_MSG);
    }

    LOG_TRACE('Checking password for member: %o', doc.id);
    const { match, update } = member.checkPasswordForDoc(doc, creds.pass);

    if (!match) {
      LOG_TRACE("Password doesn't match for user: %o", doc.id);
      throw new UnauthorizedError(USER_OR_PASS_MSG);
    }

    if (update) {
      LOG_TRACE('Attempting to update password hash');
      const { error, value: toUpdate } = member.dbUpdateSchema.validate(
        member.applyPasswordHash({
          email: doc.get('email'),
          password: creds.pass,
        }),
      );
      if (error) {
        LOG_ERR('Unable to update password hash:', error);
      } else {
        LOG_TRACE('Updating member:', toUpdate);
        await tx.update(doc.ref, toUpdate);
      }
    }
    return new RegisteredUser({ di: req.di, model: doc });
  });
};
exports.jwtProvider = audience => async function jwtProvider(req) {
  const token = getToken(req);

  try {
    const payload = await req.di.verifyJwt(
      { token, audience },
      req.di,
    );
    return payload.sub
      ? new RegisteredUser({ di: req.di, payload })
      : new GuestUser({ di: req.di });
  } catch (err) {
    LOG_TRACE('Unable to auth:', err);
    // TODO better error messages for consumers here would be useful
    throw new UnauthorizedError('Invalid, or incorrect token');
  }
};

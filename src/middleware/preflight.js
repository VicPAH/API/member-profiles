const { sendCorsHeaders } = require('@VicPAH/gcloud-functions-cors-middleware');
const { handleHttpErrors } = require('@VicPAH/gcloud-functions-http-errors-middleware');
const { checkAndSendMethod } = require('@VicPAH/gcloud-functions-http-method-middleware');

exports.defaultPreflights = (allowedMethods, options) => [
  handleHttpErrors,
  sendCorsHeaders,
  checkAndSendMethod(allowedMethods, options),
];
